var renderers = [];
var rendererNames = {
    "Canvas (Fallback)": 0,
    "WebGL (Standard)": 1
};

var font;

var scene;
var camera;
var cameras = [];
var cameraNames = {
    "Perspective": 0,
    "Orthographic": 1
};
var renderer;
var canvas;
var canvas1;
var $canvas;
var $canvas1;
var container;
var $container;

var displayRatio = 0.6;

var controls;

var gui;
var fa;
var guiText;

var materials = [];
var materialNames = {
    "Basic": 0,
    "Lambert": 1,
    "Phong": 2,
    "Toon": 3
};

var effect;

var mesh;
var meshes = [];
var meshNames = {
    "Torus": 0,
    "Cube": 1,
    "Text": 2,
    "rastbuegel.stl": 3,
    "bucky.stl": 4,
    "pin.stl": 5,
    "nefertiti.stl": 6,
    "mesh.stl": 7,
    "trichter.stl": 8
};
var lightAmbient;
var lightKey;
var lightFill;
var lightBack;
var keyHelper;
var fillHelper;
var backHelper;

var Settings = {
    renderer: 1,
    camera: 0,
    autoRotate: true,
    lightHelpers: false,
    mesh: 0,
    text: "Text",
    color: "#6d7b87",
    material: 2,
    shine: 300,
    flatShading: false,
    wireframe: false,
    olEnabled: false,
    olColor: "#000000",
    olSize: 0.005,
    olAlpha: 1

};

$( document ).ready(function() {

    canvas = document.getElementById("display");
    canvas1 = document.getElementById("display1");
    $canvas = $(canvas);
    $canvas1 = $(canvas1);

    container = document.getElementById("displayContainer");
    $container = $(container);

    scene = new THREE.Scene();

    setupRenderers();
    setupCameras();
    setRenderer();

    effect = new THREE.OutlineEffect( renderer );

    window.addEventListener( 'resize', resize, false );

    setupMaterials();
    setupMeshes();
    setMaterials();
    addLights();

    setupGUI();

    setMesh();

    render();


});

function setupRenderers() {

    renderers[0] = new THREE.CanvasRenderer({
        alpha: true,
        canvas: canvas,
        antialias: true
    });

    renderers[1] = new THREE.WebGLRenderer({
        alpha: true,
        canvas: canvas1,
        antialias: true
    });

}

function setRenderer() {
    renderer = renderers[Settings.renderer];
    if (Settings.renderer == 0) {
        $canvas1.hide();
        $canvas.show();
    } else {
        $canvas.hide();
        $canvas1.show();
    }
    setCamera();
    resize();
}
function setupCameras() {
    cameras[0] = new THREE.PerspectiveCamera( 40, 600 / 400, 0.1, 1000 );
    cameras[0].position.z = 8;
    cameras[0].ctrl = setupControls(cameras[0]);
    cameras[0].ctrl.enabled = false;
    cameras[1] = new THREE.OrthographicCamera( -5, 5, 5 * displayRatio, -5 * displayRatio, 0.001, 1000);
    cameras[1].position.z = 7;
    cameras[1].ctrl = setupControls(cameras[1]);
    cameras[1].ctrl.enabled = false;
}
function setupMaterials() {
    materials[0] = new THREE.MeshBasicMaterial({
        color: Settings.color
    });
    materials[1] = new THREE.MeshLambertMaterial({
        color: Settings.color
    });
    materials[2] = new THREE.MeshPhongMaterial({
        color: Settings.color
    });
    materials[3] = new THREE.MeshToonMaterial({
        color: Settings.color
    });
}
function setupMeshes() {
    meshes[0] = new THREE.Mesh( new THREE.TorusGeometry(2,0.5,20,50), materials[Settings.material] );
    meshes[1] = new THREE.Mesh( new THREE.BoxGeometry(2,2,2,1,1,1), materials[Settings.material] );
    var fontLoader = new THREE.FontLoader();
    fontLoader.load( 'fonts/gentilis_bold.typeface.json', function ( response ) {
        font = response;
        var size = 2;
        var height = 0.5;
        var geo = new THREE.TextGeometry( ".", {
            font: font,
            size: size,
            height: height,
            bevelEnabled: false
        });
        geo.center();

        meshes[2] = new THREE.Mesh(geo, materials[Settings.material]);

        setText();

    });
    var stlLoader = new THREE.STLLoader();
    stlLoader.load( 'models/rastbuegel.stl', function ( geometry ) {
        centerAndScale(geometry);
        meshes[3] = new THREE.Mesh( geometry, materials[Settings.material] );
        meshes[3].rotation.x = -(Math.PI / 2);
        console.log("rastbuegel.stl loaded");
    });
    var stlLoader2 = new THREE.STLLoader();
    stlLoader2.load( 'models/bucky.stl', function ( geometry ) {
        centerAndScale(geometry);
        meshes[4] = new THREE.Mesh( geometry, materials[Settings.material] );
        meshes[4].rotation.x = -(Math.PI / 2);
        console.log("bucky.stl loaded");
    });
    var stlLoader3 = new THREE.STLLoader();
    stlLoader3.load( 'models/pin.stl', function ( geometry ) {
        centerAndScale(geometry);
        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
        meshes[5] = new THREE.Mesh( geometry, materials[Settings.material] );
        meshes[5].rotation.x = -(Math.PI / 2);
        console.log("pin.stl loaded");
    });
    var stlLoader4 = new THREE.STLLoader();
    stlLoader4.load( 'models/nefertiti.stl', function ( geometry ) {
        centerAndScale(geometry);
        meshes[6] = new THREE.Mesh( geometry, materials[Settings.material] );
        meshes[6].rotation.x = -(Math.PI / 2);
        console.log("nefertiti.stl loaded");
    });
    var stlLoader5 = new THREE.STLLoader();
    stlLoader5.load( 'models/mesh.stl', function ( geometry ) {
        centerAndScale(geometry);
        meshes[7] = new THREE.Mesh( geometry, materials[Settings.material] );
        meshes[7].rotation.x = -(Math.PI / 2);
        console.log("mesh.stl loaded");
    });
    var stlLoader6 = new THREE.STLLoader();
    stlLoader6.load( 'models/trichter.stl', function ( geometry ) {
        centerAndScale(geometry);
        meshes[8] = new THREE.Mesh( geometry, materials[Settings.material] );
        meshes[8].rotation.x = -(Math.PI / 2);
        console.log("trichter.stl loaded");
    });
}


function centerAndScale(geometry) {
    geometry.computeVertexNormals();
    geometry.computeFaceNormals();
    geometry.center();

    var max = geometry.boundingBox.max;
    var largestSize = Math.max(max.x,max.y,max.z);
    var ratio = 2 / largestSize;
    geometry.scale(ratio,ratio,ratio);
}
function setMesh() {
    if (mesh) {
        scene.remove(mesh)
    }
    mesh = meshes[Settings.mesh];
    mesh.material = materials[Settings.material];
    if (Settings.mesh == 2) {
        guiText = fa.add(Settings, 'text');

        guiText.onChange(function () {
            setText();
        });
    } else {
        if (guiText) {
            fa.remove(guiText);
            guiText = null;
        }
    }
    scene.add(mesh);
}
function setText() {
    var size = 2;
    var height = 0.5;
    var geo = new THREE.TextGeometry( Settings.text, {
        font: font,
        size: size,
        height: height,
        bevelEnabled: false
    });

    geo.center();

    meshes[2].geometry = geo;
}

function addLights() {
    lightAmbient = new THREE.AmbientLight( 0x333333);
    lightKey = new THREE.DirectionalLight( 0xffffff, 1 );
    lightKey.position.x = 1.5;
    lightKey.position.y = 3;
    lightKey.position.z = 5;
    keyHelper = new THREE.DirectionalLightHelper(lightKey, 1);
    keyHelper.enabled = false;
    lightFill = new THREE.DirectionalLight( 0xffffcc, .6 );
    lightFill.position.x = -3;
    lightFill.position.y = 1;
    lightFill.position.z = 5;
    fillHelper = new THREE.DirectionalLightHelper(lightFill, 1);
    fillHelper.enabled = false;
    lightBack = new THREE.DirectionalLight( 0xccccff, .6 );
    lightBack.position.x = -2;
    lightBack.position.y = 2;
    lightBack.position.z = -5;
    backHelper = new THREE.DirectionalLightHelper(lightBack, 1);
    backHelper.enabled = false;
    //scene.add( lightAmbient );
    scene.add( lightKey );

    scene.add( lightFill );
    scene.add( lightBack );
}

function setupControls(cam) {
    controls = new THREE.OrbitControls( cam, container );

    controls.enabled = true;
    controls.target = new THREE.Vector3();
    controls.minDistance = 0;
    controls.maxDistance = 10;
    controls.minZoom = 0;
    controls.maxZoom = 5;
    controls.minPolarAngle = 0; // radians
    controls.maxPolarAngle = Math.PI; // radians
    controls.minAzimuthAngle = - Infinity; // radians
    controls.maxAzimuthAngle = Infinity; // radians
    controls.enableDamping = false;
    controls.dampingFactor = 0.25;
    controls.enableZoom = true;
    controls.zoomSpeed = 1.0;
    controls.enableRotate = true;
    controls.rotateSpeed = 0.5;
    controls.enablePan = true;
    controls.keyPanSpeed = 7.0;	// pixels moved per arrow key push
    controls.autoRotate = true;
    controls.autoRotateSpeed = 12.0; // 30 seconds per round when fps is 60
    controls.enableKeys = true;
    controls.keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };
    controls.mouseButtons = { ORBIT: THREE.MOUSE.LEFT, ZOOM: THREE.MOUSE.MIDDLE, PAN: THREE.MOUSE.RIGHT };

    controls.target0 = controls.target.clone();
    controls.position0 = controls.object.position.clone();
    controls.zoom0 = controls.object.zoom;
    controls.enableZoom = true;



    return controls;
}

function setupGUI() {
    gui = new dat.GUI({
        autoPlace: false
    });
    $('#settings').append($(gui.domElement));
    fa = gui.addFolder('Object');
    var obj = fa.add(Settings, 'mesh', meshNames);

    var f0 = gui.addFolder('View');
    var renderer = f0.add(Settings, 'renderer', rendererNames);
    var cam = f0.add(Settings, 'camera', cameraNames);
    var autoRotate = f0.add(Settings, 'autoRotate');
    var lightHelpers = f0.add(Settings, 'lightHelpers').name("Light Helpers");

    var f1 = gui.addFolder('Material');
    var guiColor = f1.addColor(Settings, 'color');
    var guiMat = f1.add(Settings, 'material', materialNames);
    var guiWire = f1.add(Settings, 'wireframe');
    var guiShine = f1.add(Settings, 'shine').min(0).max(500).step(1);
    var guiShading = f1.add(Settings, 'flatShading');

    var f2 = gui.addFolder('Outline');
    var olEnabled = f2.add(Settings, 'olEnabled').name("enabled");
    var olColor = f2.addColor(Settings, 'olColor').name("color");
    var olSize = f2.add(Settings, 'olSize').min(0.001).max(0.1).step(0.001).name("size");
    var olAlpha = f2.add(Settings, 'olAlpha').min(0.1).max(1).step(0.01).name("alpha");

    fa.open();
    f0.open();
    f1.open();
    f2.open();
    guiShading.onChange(function () {
        console.log(materials[Settings.material].shading)
        if (Settings.flatShading) {
            materials[Settings.material].shading = THREE.FlatShading;
        } else {
            materials[Settings.material].shading = THREE.SmoothShading;
        }

    });
    renderer.onChange(function () {
        setRenderer();
    });
    guiShine.onChange(function () {
        materials[Settings.material].shininess = Settings.shine;
    });
    lightHelpers.onChange(function () {
        if (Settings.lightHelpers) {
            scene.add(keyHelper);
            scene.add(fillHelper);
            scene.add(backHelper);
        } else {
            scene.remove(keyHelper);
            scene.remove(fillHelper);
            scene.remove(backHelper);
        }
    });
    guiWire.onChange(function () {
        materials[Settings.material].wireframe = Settings.wireframe;
    });
    obj.onChange(function() {
        setMesh();
    });
    cam.onChange(function() {
        setCamera();
    });
    olEnabled.onChange(function() {
        setOutline();
    });
    olColor.onChange(function() {
        setOutline();
    });
    olSize.onChange(function() {
        setOutline();
    });
    olAlpha.onChange(function() {
        setOutline();
    });
    guiColor.onChange(function() {
        setMaterials()
    });
    guiMat.onChange(function() {
        setMaterials()
    });
}

function setMaterials() {
    materials[Settings.material].color.set(Settings.color);
    materials[Settings.material].wireframe = Settings.wireframe;
    materials[Settings.material].shininess = Settings.shine;
    if (Settings.flatShading) {
        materials[Settings.material].shading = THREE.FlatShading;
    } else {
        materials[Settings.material].shading = THREE.SmoothShading;
    }
    setOutline();
    meshes[Settings.mesh].material = materials[Settings.material];

    if (Settings.renderer == 1) {
        for (var i = 0; i < materials.length; i++) {
            materials[i].overdraw = 1;
        }
    }
}

function setCamera() {
    if (camera) {
        camera.ctrl.enabled = false;
    }

    camera = cameras[Settings.camera];

    camera.ctrl.enabled = true;
    camera.updateProjectionMatrix();

    resize();
}

function setOutline() {
    materials[Settings.material].outlineParameters = {
        thickness: Settings.olSize,                     // this paremeter won't work for MultiMaterial
        color: new THREE.Color(Settings.olColor),  // this paremeter won't work for MultiMaterial
        alpha: Settings.olAlpha,                          // this paremeter won't work for MultiMaterial
        visible: Settings.olEnabled,
        keepAlive: true  // this paremeter won't work for Material in materials of MultiMaterial
    };
}

function render() {
    requestAnimationFrame( render );

    renderer.render( scene, camera );

    if (Settings.renderer == 1) {
        effect.render(scene, camera);
    }
    if (Settings.autoRotate) {
        cameras[Settings.camera].ctrl.update();
    }
}


function resize(){

    var w = $container.width();
    var h = Math.floor(w * displayRatio);

    camera.aspect = w / h;
    camera.updateProjectionMatrix();

    renderer.setSize( w, h, false );

    $container.height(h);

}

// getElementById
function $id(id) {
    return document.getElementById(id);
}

// call initialization file
if (window.File && window.FileList && window.FileReader) {
    var fileselect = $id("fileselect"),
        filedrag = $id("filedrag"),
        submitbutton = $id("submitbutton");

    // file select
    fileselect.addEventListener("change", FileSelectHandler, false);

    // is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {

        // file drop
        filedrag.addEventListener("dragover", FileDragHover, false);
        filedrag.addEventListener("dragleave", FileDragHover, false);
        filedrag.addEventListener("drop", FileSelectHandler, false);
        filedrag.style.display = "block";

        // remove submit button
        submitbutton.style.display = "none";
    }

} else {
    output("File API not available");
}

// file drag hover
function FileDragHover(e) {
    e.stopPropagation();
    e.preventDefault();
    e.target.className = (e.type == "dragover" ? "hover" : "");
}

function ParseFile(file) {



    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
            output(
                "<p>File information: <strong>" + theFile.name +
                "</strong> type: <strong>" + theFile.type +
                "</strong> size: <strong>" + theFile.size +
                "</strong> bytes</p><p>" +
                "</p><p>" +
                "</p>"
            );

            console.log(e.target);
        };
    })(file);

    // Read in the image file as a data URL.
    reader.readAsText(file);

    console.log(file);

}

// file selection
function FileSelectHandler(e) {

    // cancel event and hover styling
    FileDragHover(e);

    // fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // process all File objects
    for (var i = 0, f; f = files[i]; i++) {
        ParseFile(f);
    }

}

// output information
function output(msg) {
    var m = $id("messages");
    m.innerHTML = msg + m.innerHTML;
}