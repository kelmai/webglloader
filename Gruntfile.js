/*global module:false*/
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
        ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        // Task configuration.
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            js: {
                src: ['js/src/app.js'],
                dest: 'js/app.js'
            },
            css: {
                src: ['css/app.css'],
                dest: 'css/app.css'
            },
            cssMin: {
                src: ['css/app.min.css'],
                dest: 'css/app.min.css'
            }
        },
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                options: {
                    outputStyle: 'expanded'
                },
                files: {
                    'css/app.css': 'css/src/app.scss'
                }
            },
            min: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {
                    'css/app.min.css': 'css/src/app.scss'
                }
            }
        },
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            js: {
                src: '<%= concat.js.dest %>',
                dest: 'js/app.min.js'
            }
        },
        copy: {
            js: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        src: [
                            'node_modules/three/build/three.min.js',
                            'node_modules/three/build/three.js',
                            'node_modules/jquery/dist/jquery.min.js',
                            'node_modules/jquery/dist/jquery.js',
                            'node_modules/dat.gui/build/dat.gui.js',
                            'node_modules/dat.gui/build/dat.gui.min.js',
                            'node_modules/three/examples/js/controls/OrbitControls.js',
                            'node_modules/three/examples/js/effects/OutlineEffect.js',
                            'node_modules/three/examples/js/utils/GeometryUtils.js',
                            'node_modules/three/examples/js/loaders/STLLoader.js',
                            'node_modules/three/examples/js/renderers/CanvasRenderer.js',
                            'node_modules/three/examples/js/renderers/Projector.js',
                        ],
                        dest: 'js/lib/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            },
            css: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        src: [
                            'node_modules/normalize.css/normalize.css',
                            'node_modules/dat.gui/build/dat.gui.css'
                        ],
                        dest: 'css/lib/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            },
            fonts: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        src: [
                            'node_modules/three/examples/fonts/*.json'
                        ],
                        dest: 'fonts/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            },
            shaders: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        src: [
                            'node_modules/three/examples/js/shaders/*.js'
                        ],
                        dest: 'js/lib/shaders/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            }
        },
        watch: {
            sass: {
                files: 'css/src/**/*.scss',
                tasks: ['sass', 'concat:css', 'concat:cssMin']
            },
            js: {
                files: 'js/src/**/*.js',
                tasks: ['concat:js', 'uglify']
            }
        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    keepalive: true,
                    debug: true,
                    open: true
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-connect');

    // Default task.
    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'copy']);

};
